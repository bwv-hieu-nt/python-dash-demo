import plotly.express as px
import pandas as pd
import csv
import json

df = pd.read_csv('csv/app_sample.csv')
# df = px.data.stocks()

if __name__ == '__main__':
    # df_tup = (df, 'file.csv', None)
    df_json = df.to_json()
    print(df_json)
    print(type(df_json))

    df_frame = pd.read_json(df_json)
    print(df_frame)
    print(type(df_frame))
