from datetime import datetime, timedelta
import dash
from dash import dcc
from dash import html
from dash.dependencies import Input, Output
import plotly.express as px
import pandas as pd

app = dash.Dash(__name__)

df = pd.read_csv('csv/graph_sample.csv')
df['DateTime'] = pd.to_datetime(df['DateTime'], format='%Y%m%d %H:%M:%S.%f')
start_date_range = df['DateTime'].min().strftime('%Y-%m-%d')
end_date_range = df['DateTime'].max().strftime('%Y-%m-%d')

app.layout = html.Div(children=[
    dcc.DatePickerRange(
        id='date-picker-range',
        start_date=start_date_range,
        end_date=end_date_range,
        minimum_nights=0,
        display_format='YYYY/MM/DD'
    ),
    dcc.Graph(id='graph-line')
])


@app.callback(
    Output('graph-line', 'figure'),
    Input('date-picker-range', 'start_date'),
    Input('date-picker-range', 'end_date')
)
def update_figure(start_date, end_date):
    if start_date is not None and end_date is not None:
        start_date = datetime.fromisoformat(start_date)
        end_date = datetime.fromisoformat(end_date) + timedelta(days=1)
        filtered_df = df[(start_date <= df['DateTime']) & (df['DateTime'] <= end_date)]
        fig = px.line(filtered_df, x='DateTime', y=['DATA 1', 'DATA 2', 'DATA 3', 'DATA 4'])
        fig.update_layout(transition_duration=500)
        return fig


if __name__ == '__main__':
    app.run_server(debug=True)
