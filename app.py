from datetime import datetime, timedelta
import io
import base64
import dash_bootstrap_components as dbc
import dash
from dash import dcc
from dash import html
from dash.dependencies import Input, Output, State
from dash import dash_table
import plotly.express as px
import pandas as pd

external_stylesheets = [
    # Dash CSS
    # 'https://codepen.io/chriddyp/pen/bWLwgP.css',
    # Loading screen CSS
    'https://codepen.io/chriddyp/pen/brPBPO.css',
    {
        'href': 'https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css',
        'rel': 'stylesheet',
        'integrity': 'sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn',
        'crossorigin': 'anonymous'
    },
    dbc.themes.BOOTSTRAP
]

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

PAGE_SIZE = 5

app.layout = dbc.Container([
    dcc.Store(id='memory-data'),
    dbc.Row([
        dbc.Col([
            dbc.Label('Choose file upload'),
            html.Span('(Only accept csv file)', style={'color': '#ff0000b8', 'fontSize': '0.7rem'}),
            dcc.Upload(
                id='data-upload',
                children=html.Div([
                    'Drag and Drop or ',
                    html.A('Select Files', className='select-file')
                ]),
                style={
                    'width': '100%',
                    'height': '60px',
                    'lineHeight': '60px',
                    'borderWidth': '1px',
                    'borderStyle': 'dashed',
                    'borderRadius': '5px',
                    'textAlign': 'center',
                    'margin': '10px'
                },
                # Allow multiple files to be uploaded
                multiple=False,
            ),
            html.Div(id='output-data-file'),
        ], md=12)
    ]),
    html.Br(),
    dbc.Row([
        dbc.Col([
            dbc.Label('Select data type to compare'),
            dcc.Dropdown(
                id='data-selected',
                options=[],
                multi=True
            ),
        ], md=6)
    ]),
    html.Br(),
    dbc.Row([
        dbc.Col([
            dbc.Label('Enter the comparison interval'),
            dcc.DatePickerRange(
                id='date-picker-range',
                display_format='YYYY/MM/DD',
                className='d-block'
            ),
        ], md=6)
    ]),
    html.Br(),
    dbc.Row(dbc.Col(dcc.Graph(id='app-graph'), md=12)),
    html.Br(),
    dbc.Row([
        dbc.Col([
            dash_table.DataTable(
                id='datatable-paging-page-count',
                columns=[],
                page_current=0,
                page_size=PAGE_SIZE,
                page_action='none'
            )
        ], md=12)
    ])
], fluid=False)


@app.callback(
    Output('memory-data', 'data'),
    Input('data-upload', 'contents'),
    State('data-upload', 'filename')
)
def update_data_frame(file_content, file_name):
    if file_content is not None:
        content_type, content_string = file_content.split(',')
        decoded = base64.b64decode(content_string)
        result_data = {
            'file_name': file_name,
            'data_frame': None,
            'mess_error': None
        }
        try:
            if 'csv' in file_name:
                df_csv = pd.read_csv(io.StringIO(decoded.decode('utf-8')))
                result_data['data_frame'] = df_csv.to_json()
            else:
                result_data['mess_error'] = 'Only accept csv file.'
        except Exception as e:
            print(e)
            result_data['mess_error'] = 'There was an error processing this file.'
        return result_data


@app.callback(
    Output('output-data-file', 'children'),
    Input('memory-data', 'data')
)
def update_output_data_file(memory_data):
    if memory_data is not None:
        if memory_data['mess_error'] is not None:
            return memory_data['mess_error']
        elif memory_data['data_frame'] is not None:
            return memory_data['file_name']
        else:
            return ''


@app.callback(
    Output('data-selected', 'options'),
    Input('memory-data', 'data'),
    State('data-selected', 'options')
)
def update_input_select(memory_data, options):
    if memory_data is not None and memory_data['data_frame'] is not None:
        dff = pd.read_json(memory_data['data_frame'])
        options += [{'label': x, 'value': x} for x in dff.columns[1:]]
    return options


@app.callback(
    [
        Output('date-picker-range', 'start_date'),
        Output('date-picker-range', 'end_date')
    ],
    Input('memory-data', 'data')
)
def update_date_picker_range(memory_data):
    if memory_data is not None and memory_data['data_frame'] is not None:
        dff = pd.read_json(memory_data['data_frame'])
        return dff['DateTime'].min().strftime('%Y-%m-%d'), dff['DateTime'].max().strftime('%Y-%m-%d')
    else:
        return None, None


@app.callback(
    Output('app-graph', 'figure'),
    [
        Input('memory-data', 'data'),
        Input('data-selected', 'value'),
        Input('date-picker-range', 'start_date'),
        Input('date-picker-range', 'end_date'),
    ]
)
def update_figure(memory_data, data_selected, start_date, end_date):
    if memory_data is not None and memory_data['data_frame'] is not None:
        dff = pd.read_json(memory_data['data_frame'])
        dff['DateTime'] = pd.to_datetime(dff['DateTime'], format='%Y%m%d %H:%M:%S.%f')
        if start_date is not None and end_date is not None:
            df_start_date = datetime.fromisoformat(start_date)
            df_end_date = datetime.fromisoformat(end_date) + timedelta(days=1)
        else:
            df_start_date = datetime.fromisoformat(dff['DateTime'].min().strftime('%Y-%m-%d'))
            df_end_date = datetime.fromisoformat(dff['DateTime'].max().strftime('%Y-%m-%d'))
        filtered_df = dff[(df_start_date <= dff['DateTime']) & (dff['DateTime'] <= df_end_date)]
        if data_selected is not None and len(data_selected) != 0:
            y_data_type = data_selected
        else:
            y_data_type = dff.columns[1:]
        fig = px.line(filtered_df, x='DateTime', y=y_data_type)
        fig.update_layout(transition_duration=500)
        return fig
    else:
        return {}


@app.callback(
    Output('datatable-paging-page-count', 'columns'),
    Output('datatable-paging-page-count', 'page_action'),
    Output('datatable-paging-page-count', 'page_count'),
    Input('memory-data', 'data'),
    State('datatable-paging-page-count', 'columns'),
    State('datatable-paging-page-count', 'page_action'),
    State('datatable-paging-page-count', 'page_count')
)
def update_data_table(memory_data, columns, page_action, page_count):
    if memory_data is not None and memory_data['data_frame'] is not None:
        dff = pd.read_json(memory_data['data_frame'])
        columns += [{"name": i, "id": i} for i in dff.columns]
        page_action = 'custom'
        total_page_count = len(dff) % PAGE_SIZE
        page_count = total_page_count if total_page_count == 0 else total_page_count + 1
    return columns, page_action, page_count


@app.callback(
    Output('datatable-paging-page-count', 'data'),
    Input('memory-data', 'data'),
    Input('datatable-paging-page-count', 'page_current'),
    Input('datatable-paging-page-count', 'page_size'))
def update_data_table(memory_data, page_current, page_size):
    if memory_data is not None and memory_data['data_frame'] is not None:
        dff = pd.read_json(memory_data['data_frame'])
        return dff.iloc[
               page_current * page_size:(page_current + 1) * page_size
           ].to_dict('records')


if __name__ == '__main__':
    app.run_server(debug=True)
